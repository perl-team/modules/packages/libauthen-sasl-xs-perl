Source: libauthen-sasl-xs-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libauthen-sasl-perl <!nocheck>,
               libdevel-checklib-perl,
               libsasl2-dev,
               libsasl2-modules <!nocheck>,
               libtest-simple-perl <!nocheck>,
               perl-xs-dev,
               perl:native
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libauthen-sasl-xs-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libauthen-sasl-xs-perl.git
Homepage: https://metacpan.org/release/Authen-SASL-XS
Rules-Requires-Root: no

Package: libauthen-sasl-xs-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends},
         libauthen-sasl-perl
Description: Perl XS extension to glue Perl SASL to Cyrus SASL
 SASL is a generic mechanism for authentication used by several network
 protocols. Authen::SASL::XS provides an implementation framework that all
 protocols should be able to share.
 .
 The XS framework makes calls into the existing libsasl.so resp. libsasl2
 shared library to perform SASL client connection functionality, including
 loading existing shared library mechanisms.
 .
 libauthen-sasl-xs-perl is the successor of libauthen-sasl-cyrus-perl.
